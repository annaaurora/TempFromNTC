/*
  TempFromNTC - Library for getting the temperature from an NTC on Arduinos. Created by Anna Aurora Neugebauer, 2023-01-16.
*/
#define TempFromNTC

#include "Arduino.h"

class TempFromNTC
{
  public:
    TempFromNTC(String ntcPin);
    double analog2Kelvin(float value);
    double kelvin2Celsius(double kelvin);
    double readTempKelvin();
    float readOhm();
    double readTempCelsius();
  private:
    // Which analog pin should be read.
    int _ntcPin;
};
