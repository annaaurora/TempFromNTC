/*
  TempFromNTC - Library for getting the temperature from an NTC on Arduinos. Created by Anna Aurora Neugebauer, 2023-01-16.
*/
#include "Arduino.h"
#include <TempFromNTC.h>
#include <math.h>

TempFromNTC::TempFromNTC(String ntcPin) {
  _ntcPin = ntcPin;
  const int ntcWiderstand = 10000; // NTC-Widerstand mit 10 kOhm
  const int MAX_ANALOG_VALUE = 1023;
}

double analog2Kelvin(float value) {
  double kelvin = log(((10240000/value) - ntcWiderstand));
  
  kelvin = 1 / (0.001129148 + (0.000234125 * kelvin) + (0.0000000876741 * kelvin * kelvin * kelvin));
 
  return kelvin;
}

double kelvin2Celsius(double kelvin) {
  return kelvin - 273.15;
}

void printValue(double kelvin, double celsius, float ohm) {
  Serial.print(millis());
  Serial.print("\t\t|");

  Serial.print(kelvin);
  Serial.print(" K \t|");

  Serial.print(celsius);
  Serial.print(" °C\t|");

  Serial.print(ohm);
  Serial.println(" Ohm");
}

double readTempKelvin() {
  float analogValue = analogRead(_ntcPin);

  // Konvertieren des analogen Wertes in ein Widerstandswert
  float resistorValue = (MAX_ANALOG_VALUE / analogValue) - 1;
  resistorValue = ntcWiderstand / resistorValue;

  double kelvin = analog2Kelvin(analogValue);
  return kelvin;
}

float readOhm() {
  float analogValue = analogRead(_ntcPin);

  // Konvertieren des analogen Wertes in ein Widerstandswert
  float resistorValue = (MAX_ANALOG_VALUE / analogValue) - 1;
  resistorValue = ntcWiderstand / resistorValue;

  return resistorValue;
}

double readTempCelsius() {
  double kelvin = readTempKelvin();
  double celsius = kelvin2Celsius(kelvin);
  return celsius;
}
